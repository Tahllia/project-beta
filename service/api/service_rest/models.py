from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_ID = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class ServiceAppoitment(models.Model):
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician, related_name="service_tech", on_delete=models.CASCADE,)  
    appt_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.TextField(max_length=250, unique=False)
    completed = models.BooleanField(default=False)
    purchased = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.customer}, {self.appt_date}"
