# Generated by Django 4.0.3 on 2022-09-13 21:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AutomobileVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.CharField(max_length=17, unique=True)),
                ('import_href', models.CharField(max_length=200, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='CustomerVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=200)),
                ('phone_number', models.BigIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Technician',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('employee_ID', models.CharField(max_length=200, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='ServiceAppoitment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('appt_date', models.DateTimeField()),
                ('reason', models.TextField(max_length=250)),
                ('completed', models.BooleanField(default=False)),
                ('customer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='serv_appt', to='service_rest.customervo')),
                ('technician', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='service_tech', to='service_rest.technician')),
                ('vin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='service_appoitments', to='service_rest.automobilevo')),
            ],
        ),
    ]
