from django.urls import path

from .views import(
    api_list_appts,
    api_service_detail,
    api_technicians,
    api_technician,
)


urlpatterns = [
    path("service/", api_list_appts, name="api_list_appts"),
    path("service/<int:pk>/", api_service_detail, name="api_service_detail"),
    path("technician/", api_technicians, name="api_technicians"),
    path("technician/<int:pk>", api_technician, name ="api_technician"),
]
