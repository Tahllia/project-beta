# Generated by Django 4.0.3 on 2022-09-13 01:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='phone_number',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='salesrecord',
            name='price',
            field=models.IntegerField(),
        ),
    ]
