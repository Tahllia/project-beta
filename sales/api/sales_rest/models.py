from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_ID = models.CharField(max_length=20, unique=True)

    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"id": self.employee_ID})

    def __str__(self):  
        return self.name

class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=200)
    phone_number = models.BigIntegerField()

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})

    def __str__(self):
        return self.name

class SalesRecord(models.Model):
    automobile = models.ForeignKey(AutomobileVO, related_name='records', on_delete=models.CASCADE)
    sales_person = models.ForeignKey(SalesPerson, related_name='records', on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, related_name='records', on_delete=models.CASCADE)
    price = models.BigIntegerField()

    def get_api_url(self):
        return reverse("api_sales_record", kwargs={"pk": self.id})

