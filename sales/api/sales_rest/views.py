from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json

from .models import AutomobileVO, SalesPerson, Customer, SalesRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'name',
        'address',
        'phone_number',
        'id'
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'name',
        'employee_ID',
        'pk',
    ]

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'automobile',
        'sales_person',
        'customer',
        'price',
        'id'
    ]

    encoders = {
        'automobile': AutomobileVOEncoder(),
        'customer' : CustomerEncoder(),
        'sales_person' : SalesPersonEncoder(),
    }

    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin}


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message" : "Could not create customer"},
                status = 400,
            )
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: #PUT
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=pk)

            props = ['name', 'address', 'phone_number']
            for prop in props:
                if prop in content:
                    setattr(customer, prop, customer[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response

@require_http_methods(["GET", "POST"])
def api_sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message" : "Could not create sales_person"},
                status = 400,
            )
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_person(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(employee_ID=pk)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(employee_ID=pk)
            sales_person.delete()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: #PUT
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(employee_ID=pk)

            props = ['name', 'employee_ID']
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, sales_person[prop])
            sales_person.save()
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message" : "Does not exist"},
                status = 400,
            )
            return response


@require_http_methods(["GET", "POST"])
def api_sales_record(request):
    if request.method == "GET":
        sales_record = SalesRecord.objects.all()
        return JsonResponse(
            {"sales_record": sales_record},
            encoder=SalesRecordEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            vin = content['automobile']
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
            
            employee_ID = content["sales_person"]
            sales_person = SalesPerson.objects.get(employee_ID=employee_ID)
            content["sales_person"] = sales_person
        
            id = content["customer"]
            customer = Customer.objects.get(pk=id)
            content["customer"] = customer

            sales_record = SalesRecord.objects.create(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message" : e},
                status = 400,
            )
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_records(request, pk):
    if request.method == "GET":
        try:
            model = SalesRecord.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = SalesRecord.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            model = SalesRecord.objects.get(id=pk)
            props = ['automobile', 'sales_person', 'customer', 'price']
            for prop in props:
                if prop in content:
                    setattr(model, prop, content[prop])
            model.save()
            return JsonResponse(
                model,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET"])
def list_sales_person_records(request, employee_ID):
    sales_record = SalesRecord.objects.filter(sales_person=employee_ID)
    return JsonResponse(
        {"sales_record": sales_record},
        encoder=SalesRecordEncoder
    )