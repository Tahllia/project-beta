import React from 'react';


class ServiceHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appoitments: [],
            appoitment: "",
            vins: [],
            vin: "",
        };
        this.handlesVINChange = this.handlesVINChange.bind(this);

    };

    async handlesVINChange(event) {
        const value = event.target.value;
        this.setState({ vin: value });
        const serviceUrl = 'http://localhost:8080/api/service/';
        const serviceResponse = await fetch(serviceUrl);

        if (serviceResponse.ok) {
            let filteredServices = [];
            const servicesData = await serviceResponse.json();
            for (let appoitment of servicesData.appoitments) {
                console.log("appoitment.vin:*****", appoitment.vin)
                console.log("this.state.vin:*********", this.state.vin)
                if (appoitment.vin == this.state.vin) {
                    filteredServices.push(appoitment);
                    console.log("Matched:***" )
                }
            }
            this.setState({ appoitments: filteredServices })
            
        }
    }


    render() {
        return (
            <>
                <h1>Sales Person History</h1>
                <div className="mb-3">
                    <div className="input-group mb-3">
                        <input type="text" className="form-control" onChange={this.handlesVINChange} value={this.state.vin} 
                        placeholder="Enter Valid VIN" 
                        aria-label="Enter Valid VIN" aria-describedby="basic-addon2"/>
                    </div>
                </div>
                <div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">VIN</th>
                                <th scope="col">Customer name</th>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Technician</th>
                                <th scope="col">Reason</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.appoitments.map(appoitment => {
                                return (
                                    <tr key={appoitment.id}>
                                        <td>{appoitment.vin}</td>
                                        <td>{appoitment.customer}</td>
                                        <td>{appoitment.appt_date.split("T")[0]}</td>
                                        <td>{appoitment.appt_date.split("T")[1].split("+")[0]}</td>
                                        <td>{appoitment.technician.name}</td>
                                        <td>{appoitment.reason}</td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        )
    };
}
export default ServiceHistory;
