import React from 'react';

class VehicleList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vehicles: [],            
        };

    };
    
    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
    
        if (response.ok) {
            const data = await response.json();
            this.setState({vehicles: data.models})
            console.log(this.state.vehicles);
        };
    };

  render() {
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Manufacturer</th>
                    <th scope="col">Picture</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.vehicles.map(vehicle => {
                          return (
                          <tr key={vehicle.id}>
                            <td>{vehicle.name}</td>
                            <td>{vehicle.manufacturer.name}</td>
                            <td><img src={vehicle.picture_url} className="img-thumbnail"/></td>
                          </tr>
                          );
                      })}
                </tbody>
            </table>
        </div>
    );
    };
};
export default VehicleList;