import React from 'react';

class SalesList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            records: [],            
        };

    };
    
    async componentDidMount() {
        const recordsUrl = 'http://localhost:8090/api/records/';
        const recordsResponse = await fetch(recordsUrl);
    
        if (recordsResponse.ok) {
            const recordsData = await recordsResponse.json();
            recordsData.records = recordsData.sales_record;
            this.setState({records: recordsData.records})
            console.log(this.state.records);
        };
    };

  render() {
    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                    <th scope="col">Sales person</th>
                    <th scope="col">Employee Number</th>
                    <th scope="col">Purchaser's Name</th>
                    <th scope="col">VIN</th>
                    <th scope="col">Sale price</th>
                    </tr>
                </thead>
                <tbody>
                {this.state.records.map(record => {
                          return (
                          <tr key={record.id}>
                            <td>{record.sales_person.name}</td>
                            <td>{record.sales_person.employee_ID}</td>
                            <td>{record.customer.name}</td>
                            <td>{record.automobile}</td>
                            <td>{record.price}</td>
                          </tr>
                          );
                      })}
                </tbody>
            </table>
        </div>
    );
    };
};
export default SalesList;