import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import SalesPersonHistory from './SalesPersonHistory'
import SalesList from './SalesList'
import ManufacturerForm from './ManufacturerForm'
import ManufacturerList from './ManufacturerList'
import VehicleForm from './VehicleForm'
import VehicleList from './VehicleList'
import AutomobileForm from './AutomobileForm'
import AutomobileList from './AutomobileList'


import ServiceList from "./Services"
import ServiceHistory from "./ServiceHistory"


import NewServiceForm from "./ServiceForm" //in testing, may need to refactor the view to allow new VINs
import TechnicianForm from "./TechnicianForm"



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/sales/new" element={<SalesPersonForm />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/records/new" element={<SalesRecordForm />} />
          <Route path="/sales/history" element={<SalesPersonHistory />} />
          <Route path="/sales/list" element={<SalesList />} />

          <Route path="/service/new" element={<NewServiceForm />} />
          <Route path="/service/list" element={<ServiceList />} />
          <Route path="/service/history" element={<ServiceHistory />} />

  
          <Route path="/technician/new" element={<TechnicianForm />} /> //not ready
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/manufacturers/list" element={<ManufacturerList />} />
          <Route path="/vehicles/new" element={<VehicleForm />} />
          <Route path="/vehicles/list" element={<VehicleList />} />
          <Route path="/automobile/new" element={<AutomobileForm />} />
          <Route path="/automobile/list" element={<AutomobileList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
