import React, {Component} from 'react';
// import Checkbox from './Checkbox';

class ServiceList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appoitments: [],
            appt_ID: "",            
        };
        this.handleDelete = this.handleDelete.bind(this);
        };



    async componentDidMount() {
        const servicesUrl = 'http://localhost:8080/api/service/';
        const serviceResponse = await fetch(servicesUrl);
    
        if (serviceResponse.ok) {
            const serviceData = await serviceResponse.json();
            this.setState({appoitments: serviceData.appoitments})
            console.log("*****&&&&&&",this.state.appoitments);
        };
    };
    async handleDelete(event){
        event.preventDefault();
        console.log("event.target.value",event.target.value)
        const serviceUrl = `http://localhost:8080/api/service/${event.target.value}`;
        const fetchConfig = {
            method: "delete"
            };

        const response = await fetch(serviceUrl, fetchConfig);
    }

    




render() {
return (
    <div>
        <table className="table table-striped">
            <thead>
                <tr>
                <th scope="col">VIN</th>
                <th scope="col">Customer name</th>
                <th scope="col">Date</th>
                <th scope="col">Time</th>
                <th scope="col">Technician</th>
                <th scope="col">Reason</th>
                <th scope="col">VIP</th>
                </tr>
            </thead>
            <tbody>
            {this.state.appoitments.map(appoitments => {
                        return (
                        <tr key={appoitments.id}>
                        <td>{appoitments.vin}</td>
                        <td>{appoitments.customer}</td>
                        <td>{appoitments.appt_date.split("T")[0]}</td>
                        <td>{appoitments.appt_date.split("T")[1].split("+")[0]}</td>
                        <td>{appoitments.technician.name}</td> 
                        <td>{appoitments.reason}</td>
                        <td>{}</td>
                        <td>
                        <button onClick={this.handleDelete} title="Cancel" value={appoitments.id} >Cancel</button>
                        {/* <button onClick={this.handleDelete} title="Finshed" value={appoitments.id} >Finished</button> */}
                        </td>
                        </tr>

                        );
                    })}
            </tbody>
        </table>
    </div>
);
};
};
export default ServiceList;