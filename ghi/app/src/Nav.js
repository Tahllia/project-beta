import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="sales" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/sales/new">Add Sales Person</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/customers/new">Add Customer</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/records/new">Add Sales Record Form</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/sales/list">All Sales</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/sales/history">Sales History</NavLink>
                  </li>
                </ul>
            </li>
            
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="service" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service
              </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><NavLink className="dropdown-item" aria-current="page" to="/technician/new">Add Technician</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="/service/new">Add Service</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="/service/list">Show all Services</NavLink></li>
                  <li><NavLink className="dropdown-item" aria-current="page" to="/service/history">Service History</NavLink></li>
                </ul>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="inventory" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </a>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/new">Add A Manufacturer</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/list">Manufacturer List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/vehicles/new">Add a Vehicle Model</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/vehicles/list">Vehicle List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/automobile/new">Add a Automobile</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="dropdown-item" aria-current="page" to="/automobile/list">Automobile List</NavLink>
                  </li>
                </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
