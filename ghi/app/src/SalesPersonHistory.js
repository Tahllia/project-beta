import React from 'react';


class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            records: [],
            salesPeople: [],
            salesPerson: '',     
        };
        this.handleChange = this.handleChange.bind(this);
        this.handlesSalesPersonChange = this.handlesSalesPersonChange.bind(this);

    };

    async handlesSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({salesPerson: value});
        const recordsUrl = 'http://localhost:8090/api/records/';
        const recordsResponse = await fetch(recordsUrl);
    
        if (recordsResponse.ok) {
            let filteredRecords = [];
            const recordsData = await recordsResponse.json();
            for (let record of recordsData.sales_record){
                // console.log("record.sales_person.pk:", record.sales_person.pk)
                // console.log("this.state.salesPerson:", this.state.salesPerson)
                if (record.sales_person.pk == this.state.salesPerson) {
                    filteredRecords.push(record);
                }
            }
            this.setState({records: filteredRecords})
            // console.log(this.state.records)
        }
    }

    async handleChange(event) {
        const value = event.target.value;
        this.setState({salesPerson: value});
        const data = {...this.state};
        delete data.salesPeople;
        delete data.records;
        console.log("AFTER DATA DELETES:", data);

    }
    async componentDidMount() {
        const salesUrl = 'http://localhost:8090/api/sales';
        const salesResponse = await fetch(salesUrl);

        if (salesResponse.ok) {
            const salesData = await salesResponse.json();
            salesData.salesPeople = salesData.sales_people;
            this.setState({salesPeople: salesData.salesPeople});
            };
        };

  render() {
    return (
        <>
            <h1>Sales Person History</h1>
                <div className="mb-3">
                        <select onChange={this.handlesSalesPersonChange} value={this.state.salesPerson} required name="salesPerson" id="salesPerson" className="form-select">
                            <option value="">Choose a sales person</option>
                            {this.state.salesPeople.map(person => {
                                    return (
                                    <option key={person.pk} value={person.pk}>
                                        {person.name}
                                    </option>
                                    );
                                })}
                        </select>
                </div>
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">Sales person</th>
                        <th scope="col">Employee Number</th>
                        <th scope="col">Purchaser's Name</th>
                        <th scope="col">VIN</th>
                        <th scope="col">Sale price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.records.map(record => {
                            return (
                            <tr key={record.id}>
                                <td>{record.sales_person.name}</td>
                                <td>{record.sales_person.employee_ID}</td>
                                <td>{record.customer.name}</td>
                                <td>{record.automobile}</td>
                                <td>{record.price}</td>
                            </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );
    };
};

export default SalesRecordForm;
