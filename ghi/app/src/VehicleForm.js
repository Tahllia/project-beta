import React from 'react';

class VehicleForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name : '',
      pictureUrl : '',
      manufacturers : [],
    };
    this.handlesNameChange = this.handlesNameChange.bind(this);
    this.handlesPictureUrlnChange = this.handlesPictureUrlnChange.bind(this);
    this.handlesManufacturerChange = this.handlesManufacturerChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handlesNameChange(event) {
    const value = event.target.value;
    this.setState({name: value})
  }

  handlesPictureUrlnChange(event) {
    const value = event.target.value;
    this.setState({pictureUrl: value})
  }

  handlesManufacturerChange(event) {
    const value = event.target.value;
    this.setState({manufacturer: value})
  }



  async handleSubmit(event) {
      event.preventDefault();
      console.log({...this.state})
      const data = {...this.state};
      data.picture_url = data.pictureUrl;
      delete data.pictureUrl
      data.manufacturer_id = data.manufacturer;
      delete data.manufacturer
      delete data.manufacturers
      console.log(data)
      
      
      const recordsUrl = 'http://localhost:8100/api/models/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
          'Content-Type': 'application/json',
          },
      };
      const response = await fetch(recordsUrl, fetchConfig);
      if (response.ok) {
          await response.json();
          const cleared = {
            name: '',
            picture_url: '',
            manufacturer_id: '',
            bool: true,
          };
          this.setState(cleared);
      }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);
    // console.log(response)

    if (response.ok) {
      const data = await response.json();
      this.setState({manufacturers: data.manufacturers});
    }


  }

  render() {
    let formClass = ''
    let alertClass = 'alert alert-success d-none mb-0'
    if (this.state.bool === true) {
        formClass = 'card shadow d-none'
        alertClass = 'alert alert-success mb-0'
    }
    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form onSubmit={this.handleSubmit} id="create-conference-form" className={formClass}>
          <h1>Add a vehicle</h1>
            <div className="form-floating mb-3">
                <input onChange={this.handlesNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={this.handlesPictureUrlnChange} value={this.state.pictureUrl} placeholder="className" required type="text" name="pictureUrl" id="pictureUrl" className="form-control"/>
                <label htmlFor="name">Picture Url</label>
            </div>
            <div className="mb-3">
              <select onChange={this.handlesManufacturerChange} value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                <option value="">Choose a manufacturer</option>
                {this.state.manufacturers.map(manufacturer => {
                          return (
                          <option key={manufacturer.id} value={manufacturer.id}>
                              {manufacturer.name}
                          </option>
                          );
                      })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
          <div className={alertClass} id="success-message">
            <p>Vehicle model successfully created!</p>
          </div>
        </div>
      </div>
    </div>
    );
  }
}


export default VehicleForm;