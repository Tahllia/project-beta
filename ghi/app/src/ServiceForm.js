import React from 'react';

class NewServiceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            vin: "",
            customer: "",
            technicians: [],
            technician: "",
            appt_date: "",
            reason: "",
            completed: "False",
            purchased: "False",
        };
        this.handlesVINChange = this.handlesVINChange.bind(this);
        this.handlesCustomerChange = this.handlesCustomerChange.bind(this);
        this.handlesTechnicianChange = this.handlesTechnicianChange.bind(this);
        this.handlesApptDateChange = this.handlesApptDateChange.bind(this);
        this.handlesReaonChange = this.handlesReaonChange.bind(this);
        this.handlesCompletedChange = this.handlesCompletedChange.bind(this);   //may ned to delte if this throws errors
        this.handlesPurchasededChange = this.handlesPurchasededChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handlesVINChange(event) {
        const value = event.target.value;
        this.setState({ vin: value })
    }

    handlesCustomerChange(event) {
        const value = event.target.value;
        this.setState({ customer: value })
    }

    handlesTechnicianChange(event) {
        const value = event.target.value;
        this.setState({ technician: value })
    }

    handlesApptDateChange(event) {
        const value = event.target.value;
        this.setState({ appt_date: value })
    }
    handlesReaonChange(event) {
        const value = event.target.value;
        this.setState({ reason: value })
    }
    handlesCompletedChange(event) {
        const value = event.target.value;
        this.setState({ completed: value })
    }

    handlesPurchasededChange(event) {
        const value = event.target.value;
        // if  current state = true checked 
        this.setState({ purchased: value })
        //need to write to check if vvlaue is true
        // else sets to false
    }

    async handleSubmit(event) {
        event.preventDefault();
        console.log("this.state*****", { ...this.state })
        const data = { ...this.state };
        delete data.technicians;
        delete data.purchased;
        console.log("Data****",data)


        const serviceUrl = 'http://localhost:8080/api/service/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(serviceUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            const cleared = {
                vin: "",
                customer: "",
                technician: "",
                appt_date: "",
                reason: "",
                completed: "",
                purchased: "",
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {
        const technicianUrl = 'http://localhost:8080/api/technician/';

        const technicianResponse = await fetch(technicianUrl);

        if (technicianResponse.ok) {
            const technicianData = await technicianResponse.json();
            this.setState({ technicians: technicianData.technician });
        }
    }

render() {
return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a new service</h1>
                <form onSubmit={this.handleSubmit} id="create-service-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handlesVINChange} value={this.state.vin} placeholder="name" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">Enter a valid VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handlesCustomerChange} value={this.state.customer} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="customer">Customer Name</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={this.handlesTechnicianChange} value={this.state.technician} required name="technician" id="technician" className="form-select">
                            <option value="">Choose Technician</option>
                            {this.state.technicians.map(technician => {
                                return (
                                    <option key={technician.employee_ID} value={technician.employee_ID}>
                                        {technician.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <div className="form-floating mb-3">
                    <input type="datetime-local" onChange={this.handlesApptDateChange} value={this.state.appt_date} placeholder="date"  name="name" id="date" className="form-control" />
                    <label htmlFor="appt_date">Appoitment Date</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={this.handlesReaonChange} value={this.state.reason} placeholder="reason_text" required type="text" name="reason" id="name" className="form-control" />
                        <label htmlFor="reason">Reason for appoitment</label>
                    </div> 
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);
}
}


export default NewServiceForm;