import React from 'react';

class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
          };
          this.handleNameChange = this.handleNameChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
        }


    async handleSubmit(event) {
        event.preventDefault();
        console.log({...this.state})
        const data = {...this.state};
        
        const salesUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            await response.json();
            const cleared = {
                name: '',
                bool: true,
            };
            this.setState(cleared);
        }
    }

    render() {
    let formClass = ''
    let alertClass = 'alert alert-success d-none mb-0'
    if (this.state.bool === true) {
        formClass = 'card shadow d-none'
        alertClass = 'alert alert-success mb-0'
    }
      return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a manufacturer</h1>
            <form onSubmit={this.handleSubmit} id="create-salesPerson-form" className={formClass}>
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={alertClass} id="success-message">
              <p>Manufacturer has been created!</p>
            </div>
          </div>
        </div>
      </div>
      );
    }
  }

export default ManufacturerForm;
