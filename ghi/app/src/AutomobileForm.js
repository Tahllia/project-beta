import React from 'react';

class AutomobileForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      color : '',
      year : '',
      vin : '',
      models : [],
    };
    this.handlesColorChange = this.handlesColorChange.bind(this);
    this.handlesYearChange = this.handlesYearChange.bind(this);
    this.handlesVinChange = this.handlesVinChange.bind(this);
    this.handlesModelChange = this.handlesModelChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handlesColorChange(event) {
    const value = event.target.value;
    this.setState({color: value})
  }

  handlesYearChange(event) {
    const value = event.target.value;
    this.setState({year: value})
  }

  handlesVinChange(event) {
    const value = event.target.value;
    this.setState({vin: value})
  }

  handlesModelChange(event) {
    const value = event.target.value;
    this.setState({model: value})
  }

  async handleSubmit(event) {
      event.preventDefault();
      console.log({...this.state})
      const data = {...this.state};
      delete data.models;
      data.model_id = data.model;
      delete data.model;
      console.log(data)
      
      
      const recordsUrl = 'http://localhost:8100/api/automobiles/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
          'Content-Type': 'application/json',
          },
      };
      const response = await fetch(recordsUrl, fetchConfig);
      if (response.ok) {
          await response.json();
          const cleared = {
            color : '',
            year : '',
            vin : '',
            model_id : '',
            bool : true,
          };
          this.setState(cleared);
      }
  }

  async componentDidMount() {
    const url = 'http://localhost:8100/api/models/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({models: data.models});
    }

  }

  render() {
    let formClass = ''
    let alertClass = 'alert alert-success d-none mb-0'
    if (this.state.bool === true) {
        formClass = 'card shadow d-none'
        alertClass = 'alert alert-success mb-0'
    }
    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <div className="card-body">
            <form onSubmit={this.handleSubmit} id="create-conference-form" className={formClass}>
            <h1>Add An Automobile</h1>
              <div className="form-floating mb-3">
                  <input onChange={this.handlesColorChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="ends">Vehicle color</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={this.handlesYearChange} value={this.state.year} placeholder="year" required type="number" name="year" id="year" className="form-control"/>
                  <label htmlFor="ends">Vehicle year</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={this.handlesVinChange} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                  <label htmlFor="ends">Vehicle vin</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handlesModelChange} value={this.state.model} required name="model" id="model" className="form-select">
                  <option value="">Choose a model</option>
                  {this.state.models.map(model => {
                            return (
                            <option key={model.id} value={model.id}>
                                {model.name}
                            </option>
                            );
                        })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
            <div className={alertClass} id="success-message">
              <p>Automobile has been created!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    );
  }
}


export default AutomobileForm;