import React from 'react';

class SalesRecordForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      customers : [],
      salesPeople : [],
      automobiles : [],
      price : '',
    };
    this.handlesCustomerChange = this.handlesCustomerChange.bind(this);
    this.handlesSalesPersonChange = this.handlesSalesPersonChange.bind(this);
    this.handlesAutomobileChange = this.handlesAutomobileChange.bind(this);
    this.handlesPriceChange = this.handlesPriceChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handlesCustomerChange(event) {
    const value = event.target.value;
    this.setState({customer: value})
  }

  handlesSalesPersonChange(event) {
    const value = event.target.value;
    this.setState({SalesPerson: value})
  }

  handlesAutomobileChange(event) {
    const value = event.target.value;
    this.setState({automobile: value})
  }

  handlesPriceChange(event) {
    const value = event.target.value;
    this.setState({price: value})
  }

  async handleSubmit(event) {
      event.preventDefault();
      console.log({...this.state})
      const data = {...this.state};
      delete data.customers;
      data.sales_person = data.SalesPerson;
      delete data.SalesPerson
      delete data.salesPeople;
      delete data.automobiles;
      console.log(data)
      
      
      const recordsUrl = 'http://localhost:8090/api/records/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
          'Content-Type': 'application/json',
          },
      };
      const response = await fetch(recordsUrl, fetchConfig);
      if (response.ok) {
          await response.json();
          const cleared = {
            customer: '',
            salesPerson: '',
            automobile: '',
            price: '',
            bool: true,
          };
          this.setState(cleared);
      }
  }

  async componentDidMount() {
    const customerUrl = 'http://localhost:8090/api/customers';

    const customerResponse = await fetch(customerUrl);

    if (customerResponse.ok) {
      const customerData = await customerResponse.json();
      this.setState({customers: customerData.customers});
    }
    const salesUrl = 'http://localhost:8090/api/sales';

    const salesResponse = await fetch(salesUrl);
    // console.log(response)

    if (salesResponse.ok) {
      const salesData = await salesResponse.json();
      salesData.salesPeople = salesData.sales_people;
      this.setState({salesPeople: salesData.salesPeople});
    }
    const autoUrl = 'http://localhost:8100/api/automobiles/';

    const autoResponse = await fetch(autoUrl);
    // console.log(response)

    if (autoResponse.ok) {
      const autoData = await autoResponse.json();
      autoData.automobiles = autoData.autos;
      this.setState({automobiles: autoData.automobiles});
    }


  }

  render() {
    let formClass = ''
    let alertClass = 'alert alert-success d-none mb-0'
    if (this.state.bool === true) {
        formClass = 'card shadow d-none'
        alertClass = 'alert alert-success mb-0'
    }
    return (
      <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <form onSubmit={this.handleSubmit} id="create-conference-form" className={formClass}>
            <h1>Record a new sale</h1>
            <div className="mb-3">
              <select onChange={this.handlesCustomerChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                <option value="">Choose a Customer</option>
                {this.state.customers.map(customer => {
                          return (
                          <option key={customer.id} value={customer.id}>
                              {customer.name}
                          </option>
                          );
                      })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={this.handlesSalesPersonChange} value={this.state.salesPerson} required name="salesPerson" id="salesPerson" className="form-select">
                <option value="">Choose a sales person</option>
                {this.state.salesPeople.map(salesPerson => {
                          return (
                          <option key={salesPerson.employee_ID} value={salesPerson.employee_ID}>
                              {salesPerson.name}
                          </option>
                          );
                      })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={this.handlesAutomobileChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose a vin number</option>
                {this.state.automobiles.map(automobile => {
                          return (
                          <option key={automobile.vin} value={automobile.vin}>
                              {automobile.vin}
                          </option>
                          );
                      })}
              </select>
            </div>
            <div className="form-floating mb-3">
                <input onChange={this.handlesPriceChange} value={this.state.price} placeholder="price" required type="number" name="price" id="price" className="form-control"/>
                <label htmlFor="ends">Vehicle Price</label>
              </div>
            <button className="btn btn-primary">Create</button>
          </form>
          <div className={alertClass} id="success-message">
            <p>Sales record successfully created!</p>
        </div>
        </div>
      </div>
    </div>
    );
  }
}


export default SalesRecordForm;